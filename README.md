# Web interface for deploying DApp

## About project
This repo is a solution for CryptoBazar Hackathon task. 

## Docker

### 0. Clone repository

```
git clone https://gitlab.com/salamantos/fantom_dapp_deploying.git
cd fantom_dapp_deploying
```

### 1. Build the app

```
docker build -t fantom_dapp .
```

### 2. Run the app

```
docker run -t -i --network="host" -p 65056:65056 fantom_dapp
```

### 3. Now open web interface!

Open `http://localhost:65056` in browser