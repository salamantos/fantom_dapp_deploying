FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y git npm nodejs screen

RUN npm install axios ethereumjs-tx express body-parser

ADD script.js /
ADD index.html /
ADD resourses/* /resourses/

ENTRYPOINT node script