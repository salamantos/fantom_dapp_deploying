// node.js
const port = 65056

const axios = require('axios');
const EthereumTx = require('ethereumjs-tx') 
let express = require("express");
const bodyParser = require("body-parser");
var request_lib = require('request');
// const Eth = require("web3-eth");
// var eth = new Eth(Eth.givenProvider || 'ws://http://18.221.128.6:8080');
// var Web3 = require('web3');
// var web3 = new Web3(Web3.givenProvider || 'ws://http://18.221.128.6:8080');

let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/resourses', express.static('resourses'));

app.get('/', function(request, response){
    response.sendfile('index.html', {headers: {'Content-Type': 'text/html'}});
});

app.post("/", function (request, resp) {
    resp.setHeader("Access-Control-Allow-Origin", "*");
    let body = JSON.parse(Object.keys(request.body)[0]);
    console.log(body);

    request_lib('http://18.221.128.6:8080/account/' + body.wallet, function (error, response, bodyb) {
      // Get nonce
      console.log('nonce: ', JSON.parse(bodyb).nonce);
      let nonce = JSON.parse(bodyb).nonce

      const privateKey = Buffer.from(body.private_key, 'hex')
      const txParams = {
          nonce: nonce + 1,
          gasPrice: '0x000000000001',
          gasLimit: '0x27100',
          to: body.wallet,
          value: '0x00',
          data: '0x' + body.byte_code,
          chainId: 1 // EIP 155 chainId - mainnet: 1, ropsten: 3
      }
      console.log(txParams);

      let tx;
      let serializedTx; 
      let hexTx;
      try{
        tx = new EthereumTx(txParams) 
        tx.sign(privateKey) 
        var feeCost = tx.getUpfrontCost()
        tx.gas = feeCost
        console.log('Total Amount of wei needed:' + feeCost.toString())
        serializedTx = tx.serialize() 
        hexTx = '0x' + serializedTx.toString('hex')
      }
      catch (error){
        console.log("error EthereumTx: ", error); 
        resp.send(JSON.stringify({responce: error.toString(), code: 1}));
        return;
      }

      axios.post('http://18.221.128.6:8080/sendRawTransaction', hexTx)
      .then(function (response) {
        console.log("response.data: ", response.data); 
        resp.send(JSON.stringify({responce: response.data, code: 0}));
      }).catch(function (error) {
        console.log("error: ", error); 
        resp.send(JSON.stringify({responce: error, code: 1}));
      });
    });
  });
  
  app.listen(port, function () {
    console.log("Success! Open http://localhost:65056 in browser");
  });







//50c4bf4dde1f383a172f52cb4624f089f685e67e00c6741a3ae03826c99cf082:0xFD00A5fE03CB4672e4380046938cFe 5A18456Df4 //7c9d2f34f5869204fe8232442bc2280a613601783fab2b936cf91a054668537a:0xfd9AB87eCEdC912A63f5B8fa3b8d766 7d33Fd981
// http://18.221.128.6:8080/account/629007eb99ff5c3539ada8a5800847eacfc25727
// http://18.221.128.6:8080/sendRawTransaction
// http://18.221.128.6:8080/transactions
// http://18.221.128.6:8080/accounts
// http://18.221.128.6:8080/transaction/{}
// http://18.221.128.6:8080/account/{}

/* 
http://18.221.128.6:8080/account/0xFD00A5fE03CB4672e4380046938cFe5A18456Df4  {"address":"0xFD00A5fE03CB4672e4380046938cFe5A18456Df4","balance":9999790000000000000000,"nonce":1}
http://18.221.128.6:8080/transaction/0x68a07a9dc6ff0052e42f4e7afa117e90fb896eda168211f040da69606a2aeddc
{"root":"0x7ed3e21533e05c18ded09e02d7bf6bf812c218a3a7af8c6b5cc23b5cb4951069","transactionHash":"0x68a07a9dc6ff0052e42f4e7afa117e90fb896eda168211f040da69606a2aeddc","from":"0xfd00a5fe03cb4672e4380046938cfe5a18456df4","to":"0xfd00a5fe03cb4672e4380046938cfe5a18456df4","gasUsed":21000,"cumulativeGasUsed":21000,"contractAddress":"0x0000000000000000000000000000000000000000","logs":[],"logsBloom":"0x0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 0000000000000000000000000000","failed":false}

function getNonce(tx) {
axios.get('http://18.221.128.6:8080/account/'+tx.from)
.then(function (response) {
	tx.nonce = response.data.nonce
generateRawTx(tx, priv)
})
.catch(function (error) {
console.log(error);
});
}
*/


/* {
	"linkReferences": {},
	"object": "6060604052341561000f57600080fd5b60bb8061001d6000396000f30060606040526004361060485763ffffffff7c0100000000000000000000000000000000000000000000000000000000600035041663967e6e658114604d578063d5dcf12714606f575b600080fd5b3415605757600080fd5b605d6084565b60405190815260200160405180910390f35b3415607957600080fd5b6082600435608a565b005b60005490565b6000555600a165627a7a7230582050f5ddfb73d4c27d76695585bae3849afdbd5540108301b506bc73fa58d9da7c0029",
	"opcodes": "PUSH1 0x60 PUSH1 0x40 MSTORE CALLVALUE ISZERO PUSH2 0xF JUMPI PUSH1 0x0 DUP1 REVERT JUMPDEST PUSH1 0xBB DUP1 PUSH2 0x1D PUSH1 0x0 CODECOPY PUSH1 0x0 RETURN STOP PUSH1 0x60 PUSH1 0x40 MSTORE PUSH1 0x4 CALLDATASIZE LT PUSH1 0x48 JUMPI PUSH4 0xFFFFFFFF PUSH29 0x100000000000000000000000000000000000000000000000000000000 PUSH1 0x0 CALLDATALOAD DIV AND PUSH4 0x967E6E65 DUP2 EQ PUSH1 0x4D JUMPI DUP1 PUSH4 0xD5DCF127 EQ PUSH1 0x6F JUMPI JUMPDEST PUSH1 0x0 DUP1 REVERT JUMPDEST CALLVALUE ISZERO PUSH1 0x57 JUMPI PUSH1 0x0 DUP1 REVERT JUMPDEST PUSH1 0x5D PUSH1 0x84 JUMP JUMPDEST PUSH1 0x40 MLOAD SWAP1 DUP2 MSTORE PUSH1 0x20 ADD PUSH1 0x40 MLOAD DUP1 SWAP2 SUB SWAP1 RETURN JUMPDEST CALLVALUE ISZERO PUSH1 0x79 JUMPI PUSH1 0x0 DUP1 REVERT JUMPDEST PUSH1 0x82 PUSH1 0x4 CALLDATALOAD PUSH1 0x8A JUMP JUMPDEST STOP JUMPDEST PUSH1 0x0 SLOAD SWAP1 JUMP JUMPDEST PUSH1 0x0 SSTORE JUMP STOP LOG1 PUSH6 0x627A7A723058 KECCAK256 POP 0xf5 0xdd CREATE2 PUSH20 0xD4C27D76695585BAE3849AFDBD5540108301B506 0xbc PUSH20 0xFA58D9DA7C002900000000000000000000000000 ",
	"sourceMap": "47:202:0:-;;;;;;;;;;;;;;;;;"
} */
